﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests
{
    public class Tests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestsSimplePasses()
        {
            // Use the Assert class to test conditions
            GameObject go = new GameObject();
            RestConnection r = go.AddComponent<RestConnection>();
            Assert.IsNotNull(go);
            GameObject panel = new GameObject();
            panel.AddComponent<Dropdown>();
            go.GetComponent<RestConnection>().regionNumber = panel.GetComponent<Dropdown>();
            go.GetComponent<RestConnection>().regionNumber.ClearOptions();
            for (int i = 0; i < 15; i++)
            {
                go.GetComponent<RestConnection>().regionNumber.options.Add(new Dropdown.OptionData((i+1).ToString()));
            }
            go.GetComponent<RestConnection>().regionNumber.RefreshShownValue();
            Assert.IsTrue(go.GetComponent<RestConnection>().regionNumber.options.Count > 1);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TestsWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
