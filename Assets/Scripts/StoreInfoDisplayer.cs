﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class StoreInfoDisplayer : MonoBehaviour
{
    Store store;
    public static OnStoreSelectedEvent onStoreSelected = new OnStoreSelectedEvent();

    [SerializeField]
    Text storeName, storeAddress, storePhone, storeLat, storeLong;
    // Start is called before the first frame update
    void Start()
    {
        onStoreSelected.AddListener(StoreSelected);
    }

    void StoreSelected(Store store)
    {
        this.store = store;
        SetTextValue(storeName, store.local_nombre);
        SetTextValue(storePhone, store.local_telefono);
        SetTextValue(storeAddress, store.local_direccion);
        SetTextValue(storeLat, store.local_lat);
        SetTextValue(storeLong, store.local_lng);
    }

    void SetTextValue(Text textComp, string value)
    {
        textComp.text = value;
    }
}

public class OnStoreSelectedEvent : UnityEvent<Store>
{
}
