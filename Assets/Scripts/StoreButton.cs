﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreButton : MonoBehaviour
{
    Button button;

    bool _selected;
    public bool isSelected{get{return _selected;}}

    int _id;
    public int id{get{return _id;}}

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnSelect);
    }
    public void OnSelect()
    {
        StoreSelectorHandler.onStoreSelected.Invoke(id);
    }
    public void SetId(int id)
    {
        _id = id;
    }
    public void SetId(string id)
    {
        _id = int.Parse(id);
    }
}
