﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using UnityEngine.UI;

public class StoreSelectorHandler : MonoBehaviour
{
    public static OnStoreSelected onStoreSelected = new OnStoreSelected();

    static StoreDatabase _database;
    Store storeSelected;

    [SerializeField]
    RectTransform content;
    [SerializeField]
    StoreButton prefab;
    // Start is called before the first frame update
    void Start()
    {
        onStoreSelected.AddListener(OnStoreSelected);
        RestConnection.onStoreResponse.AddListener(SetStores);
    }

    void SetStores(StoreDatabase database)
    {
        _database = database;
        ClearOptions();
        CreateOptions(_database.response);
    }

    void OnStoreSelected(int id)
    {
        string idText = id.ToString();
        storeSelected = _database.response.First(x => x.local_id == idText);
        StoreInfoDisplayer.onStoreSelected.Invoke(storeSelected);
    }

    public void OnValueChanged(InputField input)
    {
        string curValue = input.text;
        var lowerValue = curValue.ToLower();
        var filter = _database.response;
        if(!string.IsNullOrEmpty(curValue)) filter = filter.Where(x => x.comuna_nombre.ToLower() == lowerValue || x.local_nombre.ToLower().StartsWith(lowerValue)).ToArray();
        ClearOptions();
        CreateOptions(filter);
    }

    void ClearOptions()
    {
        var children = content.GetComponentsInChildren<StoreButton>();
        var toDestroy = new List<GameObject>();
        foreach (var item in children)
        {
            item.gameObject.SetActive(false);
            toDestroy.Add(item.gameObject);
        }
        StartCoroutine(DestroyAfter(5, toDestroy));
    }

    IEnumerator DestroyAfter(float time, List<GameObject> toDestroy)
    {
        yield return new WaitForSeconds(time);
        foreach (var item in toDestroy)
        {
            Destroy(item);
        }
    }

    void CreateOptions(Store[] stores)
    {
        foreach (var item in stores)
        {
            StoreButton newButton = GameObject.Instantiate(prefab, content);
            newButton.gameObject.name = string.Concat(item.local_nombre, item.local_id);
            newButton.transform.Find("Store Name").GetComponent<Text>().text = item.local_nombre;
            newButton.transform.Find("Store ID").GetComponent<Text>().text = item.local_id;
            newButton.transform.Find("Store Commune").GetComponent<Text>().text = item.comuna_nombre;
            newButton.SetId(item.local_id);
        }
    }
}

// public class OnStoreSelected : UnityEvent<StoreButton>{}
public class OnStoreSelected : UnityEvent<int>{}
