﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugLogOnText : MonoBehaviour
{
    Text text;
    static Text textComponent;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        textComponent = text;
    }

    public static void LogMessage(string msg)
    {
        print(msg);
        textComponent.text += msg + " ";
    }

}
