﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;
using System.Net;
using System.IO;
using UnityEngine.Events;

public class RestConnection : MonoBehaviour
{
    public static OnStoreResponse onStoreResponse = new OnStoreResponse();
    StoreDatabase storesResponse = new StoreDatabase();
    Store storeSelected = new Store();
    const int ResponseOK = 200;
    public Dropdown communes;
    public Dropdown regionNumber;
    public Dropdown regionSelection;
    public Dropdown storeNames;

    void ClearOptions(params Dropdown[] d)
    {
        foreach (var item in d)
        {
            item.ClearOptions();
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        ClearOptions(communes, storeNames, regionNumber);
        for (int i = 0; i < 15; i++)
        {
            regionNumber.options.Add(new Dropdown.OptionData((i+1).ToString()));
        }
        regionNumber.RefreshShownValue();
        // StartCoroutine(GetResponse());
        Initialize();
    }

    void Initialize(){
        int value = regionNumber.value + 1;
        var id = value.ToString();
        GetStores(id);
    }

    public void OnRegionChange(Dropdown d)
    {
        int value = d.value + 1;
        var id = value.ToString();
        // StartCoroutine(GetRegionCommunes(id));
        GetStores(id);
    }

    public void OnValueChanged(InputField input)
    {
        string curValue = input.text;
        var lowerValue = curValue.ToLower();
        var filter = storesResponse.response;
        if(!string.IsNullOrEmpty(curValue)) filter = filter.Where(x => x.comuna_nombre == curValue || x.local_nombre.StartsWith(curValue)).ToArray();
        ClearOptions(storeNames);
        foreach (var item in filter)
        {
            storeNames.options.Add(new Dropdown.OptionData(GetStoreAndID(item)));
        }
        storeNames.RefreshShownValue();
    }

    public void OnCommuneChange(Dropdown d)
    {
        string textValue = d.options[d.value].text;
        print(textValue);
        var filter = storesResponse.response.Where(x => x.comuna_nombre == textValue);
        ClearOptions(storeNames);
        foreach (var item in filter)
        {
            storeNames.options.Add(new Dropdown.OptionData(GetStoreAndID(item)));
        }
        storeNames.RefreshShownValue();
    }

    public void OnStoreSelected(Dropdown d)
    {
        Store selected = new Store();
        string textValue = d.options[d.value].text;
        string[] curValue = textValue.Split('-');
        string storeName = curValue[0];
        string storeId = curValue[1];
        if(storesResponse.response.Any(x => x.local_id == storeId))
        {
            selected = storesResponse.response.First(x => x.local_id == storeId);
            storeSelected = selected;
            StoreInfoDisplayer.onStoreSelected.Invoke(storeSelected);
        }
    }

    void GetStores(string id)
    {
        ClearOptions(communes, storeNames);
        StartCoroutine(GetStoresFromServer(id));
    }

    IEnumerator GetRegionCommunes(string regionId)
    {
        WWWForm form = new WWWForm();
        form.AddField("reg_id", regionId);
        UnityWebRequest uwr = UnityWebRequest.Post("https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones", form);
        yield return uwr.SendWebRequest();
        DownloadHandler dh = uwr.downloadHandler;
        string meme = "{\"response\":" + dh.text + "}";
        StoreDatabase r = JsonUtility.FromJson<StoreDatabase>(meme);
        foreach (var item in r.response)
        {
            regionSelection.options.Add(new Dropdown.OptionData(item.comuna_nombre));
        }
    }

    IEnumerator GetStoresFromServer(string regionId)
    {
        UnityWebRequest uwr = UnityWebRequest.Get("https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region=" + regionId);
        yield return uwr.SendWebRequest();
        while(!uwr.isDone) yield return null;

        DownloadHandler dh = uwr.downloadHandler;
        DebugLogOnText.LogMessage("response " + uwr.responseCode);
        if(uwr.isNetworkError)DebugLogOnText.LogMessage("Error " + uwr.error);

        if(uwr.responseCode == ResponseOK)
        {
            string responseFormatted = "{\"response\":" + dh.text + "}";
            StoreDatabase r = JsonUtility.FromJson<StoreDatabase>(responseFormatted);
            r.response = r.response.OrderBy(x => x.comuna_nombre).ToArray();
            storesResponse = r;
            onStoreResponse.Invoke(storesResponse);
            foreach (var item in r.response)
            {
                if(!communes.options.Any(x => x.text == item.comuna_nombre)) communes.options.Add(new Dropdown.OptionData(item.comuna_nombre));
                // string storeName = item.local_nombre.Trim();
                // string storeId = item.local_id;
                // var text = string.Format("{0}-{1}", storeName, storeId);
                var text = GetStoreAndID(item);
                storeNames.options.Add(new Dropdown.OptionData(text));
            }
            communes.RefreshShownValue();
            storeNames.RefreshShownValue();
        }
    }

    string GetStoreAndID(Store store)
    {
        string storeName = store.local_nombre.Trim();
        string storeId = store.local_id;
        var text = string.Format("{0}-{1}", storeName, storeId);
        return text;
    }
}

[System.Serializable]
public class StoreDatabase
{
    public Store[] response;
}

[System.Serializable]
public class Store
{
    public string local_nombre;
    public string fecha;
    public string local_id;
    public string comuna_nombre;
    public string localidad_nombre;
    public string local_direccion;
    public string funcionamiento_hora_apertura;
    public string funcionamiento_hora_cierre;
    public string local_telefono;
    public string local_lat;
    public string local_lng;
    public string funcionamiento_dia;
    public string fk_region;
    public string fk_comuna;
}

public class OnStoreResponse : UnityEvent<StoreDatabase>{}
